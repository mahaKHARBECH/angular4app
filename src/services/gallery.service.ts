import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import "rxjs/add/operator/map";

@Injectable()
export class GalleryService{

  constructor(private http:HttpClient){

  }

  search(motCle:String, size:number, page:number) {
    // console.log(dataForm);
    return this.http.get("https://pixabay.com/api/?key=7906144-359b73e0c00d5ccc38b5b847e&q="+
      motCle+"&per_page="+size+"&page="+page)
      .map(res => res)

  }

}
