import { Component, OnInit } from '@angular/core';
import {AboutService} from "../../services/about.service";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  infos:{nom:string,email:string,tel:number}={nom:'',email:'',tel:null};
  comments=[];
  commentaire={date:null, message:""};

  //private aboutService:AboutService
  //injection de dependances par constructeur
  constructor(private aboutService:AboutService) {
    //this.aboutService=aboutService;
    this.infos=this.aboutService.getInfo();
    this.comments=this.aboutService.getAllComments();
  }

  ngOnInit() {
  }

  onAddCommentaire(c) {
    console.log(c);
    this.aboutService.addComment(c);
   // this.comments.push(c);
    this.commentaire.message="";
    this.comments=this.aboutService.getAllComments();

  }

}
