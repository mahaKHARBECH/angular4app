import { Component, OnInit } from '@angular/core';
import {GalleryService} from "../../services/gallery.service";

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  pagePhotos:any;//={totalHits:[]};
  currentPage:number=1;
  size:number=5;
  pages:Array<number>=[];
  totalPages:number;
  motCle:String="";

  constructor(private galleryService:GalleryService) { }

  ngOnInit() {
  }

  onSearch(dataForm) {
   // console.log(dataForm);
    this.galleryService.search(dataForm.motCle,this.size,this.currentPage)
      .subscribe(data =>{
      this.pagePhotos=data;
      this.totalPages=data.totalHits/this.size;
      if (data.totalHits % this.size !=0 ) ++ this.totalPages;
      this.pages = new Array(this.totalPages);
    }, err=> {console.log(err);
      })
  }

  goToPage(i) {
    this.currentPage=i+1;
    this.onSearch({motCle:this.motCle});
  }

}

